//
//  ViewController.m
//  Hello World
//
//  Created by STUDENT on 9/24/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *helloLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setHelloLabelWithString:(NSString *)text {
    if ([text isEqualToString:@""]) {
        text = @"World";
    }
    self.helloLabel.text = [NSString stringWithFormat:@"Hello %@", text];
}

- (IBAction)helloButtonTapped:(id)sender {
    [self setHelloLabelWithString:self.inputTextField.text];
}

- (IBAction)clearButtonTapped:(id)sender {
    self.inputTextField.text = @"";
}


@end
